using DocumentFormat.OpenXml.Wordprocessing;

namespace exam.Document;

public class Card
{
    public int Number { get; set; }

    public string[]? Questions { get; set; }

    public void AddCardToBody(Body body)
    {
        body.Append(Properties.GetSectionProperties());


        body.Append(Structure.GetTopParagraph());

        body.Append(Structure.GetEmptyParagraph());
        body.Append(Structure.GetCardNumber(Number));

        body.Append(Structure.GetEmptyParagraph());
        body.Append(Structure.GetDiscipline("Программирование на основе классов и шаблонов"));
        body.Append(Structure.GetLine());

        body.Append(Structure.GetEmptyParagraph());
        foreach (var question in Questions ?? Enumerable.Empty<string>())
        {
            body.Append(Structure.GetQuestion(question));
        }

        body.Append(Structure.GetLine());
        body.Append(Structure.GetEmptyParagraph());
        body.Append(Structure.GetBottomParagraphReviewed("на заседании кафедры ИУ5 №9 от «27» апреля 2023 года"));
        body.Append(Structure.GetBottomParagraphSignature("«27» апреля 2023 года"));

        body.Append(Structure.GetLine(BorderValues.Dotted));
    }
}