using DocumentFormat.OpenXml.Wordprocessing;

namespace exam.Document;

public class Properties
{
    public static int convertCmToTwips(double cm)
    {
        // Values are in twentieths of a point: 1440 twips = 1 inch; 567 twips = 1 centimeter. 
        // See more: http://officeopenxml.com/WPindentation.php
        return (int)(cm * 567);
    }

    public static SectionProperties GetSectionProperties()
    {
        var properties = new SectionProperties();
        var pageSize = new PageSize();
        pageSize.Height = (uint)convertCmToTwips(29.7);
        pageSize.Width = (uint)convertCmToTwips(21);

        var margins = new PageMargin();
        margins.Top = (int)convertCmToTwips(1);
        margins.Left = (uint)convertCmToTwips(1);
        margins.Right = (uint)convertCmToTwips(1);
        margins.Bottom = (int)convertCmToTwips(1);

        properties.Append(pageSize);
        properties.Append(margins);
        return properties;
    }
}
