using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;

namespace exam.Document;

public class Structure
{
    public static Paragraph GetTopParagraph()
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Center });

        ParagraphBorders paraBorders = new ParagraphBorders();
        BottomBorder bottom = new BottomBorder() { Val = BorderValues.Double, Color = "auto", Size = (UInt32Value)12U, Space = (UInt32Value)1U };
        paraBorders.Append(bottom);
        paraProps.Append(paraBorders);

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });

        run.AppendChild(new Text("Федеральное государственное бюджетное образовательное учреждение высшего образования"));
        run.AppendChild(new Break());
        run.AppendChild(new Text("«Московский государственный технический университет имени Н.Э. Баумана"));
        run.AppendChild(new Break());
        run.AppendChild(new Text("(национальный исследовательский университет)»"));

        return para;
    }

    public static Paragraph GetEmptyParagraph()
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Center });

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });

        run.AppendChild(new Text(" ") { Space = SpaceProcessingModeValues.Preserve });

        return para;
    }

    public static Paragraph GetBottomParagraphReviewed(string approved)
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Left });

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });
        run.AppendChild(new Text($"Билет рассмотрен и утвержден {approved}"));
        return para;
    }

    public static Paragraph GetBottomParagraphSignature(string date)
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Right });

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });
        run.AppendChild(new Text($"_____________ {date}"));
        return para;
    }

    public static Paragraph GetLine(BorderValues type = BorderValues.Single)
    {
        Paragraph para = new Paragraph();
        ParagraphProperties paraProperties = new ParagraphProperties();
        ParagraphBorders paraBorders = new ParagraphBorders();
        BottomBorder bottom = new BottomBorder() { Val = type, Color = "auto", Size = (UInt32Value)12U, Space = (UInt32Value)1U };
        paraBorders.Append(bottom);
        paraProperties.Append(paraBorders);
        para.Append(paraProperties);
        return para;
    }

    public static Paragraph GetDiscipline(string name)
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Left });

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });

        run.AppendChild(new Text("по дисциплине ") { Space = SpaceProcessingModeValues.Preserve });
        run.AppendChild(new RunProperties(new Bold()));
        run.AppendChild(new Text($"«{name}»"));
        return para;
    }

    public static Paragraph GetCardNumber(int number)
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Center });

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });

        run.AppendChild(new Text("ЭКЗАМЕНАЦИОННЫЙ БИЛЕТ №") { Space = SpaceProcessingModeValues.Preserve });
        run.AppendChild(new RunProperties(new Bold()));
        run.AppendChild(new Text($"{number}"));
        return para;
    }

    public static Paragraph GetQuestion(string question)
    {
        var para = new Paragraph();
        var paraProps = para.AppendChild(new ParagraphProperties());
        paraProps.Append(new Justification() { Val = JustificationValues.Left });

        var run = para.AppendChild(new Run());

        RunProperties runProperties = run.AppendChild(new RunProperties());
        runProperties.Append(new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" });
        runProperties.Append(new FontSize() { Val = "22" });
        run.AppendChild(new RunProperties(new Bold()));
        run.AppendChild(new Text(question));
        return para;
    }
}