#include <iostream>
#include "rectangle.h"

int main() {
    int n = 2;
    Rectangle *arr = new Rectangle[n];
    arr[0].set(1, 3);
    arr[1].set(4, 6);

    Rectangle::showRectangles(arr, n);
    std::cout << std::endl;
    std::cout << "Sum of the areas of all rectangles: "
              << Rectangle::calculateAreas(arr, n) << std::endl;
    delete[] arr;
}