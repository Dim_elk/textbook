#ifndef DECOMPOSITION_RECTANGLE_H
#define DECOMPOSITION_RECTANGLE_H

#include <iostream>

class Rectangle {
private:
    int a_, b_;
public:
    Rectangle();

    Rectangle(int a, int b);

    int getA();

    int getB();

    void setA(int a);

    void setB(int b);

    void set(int a, int b);

    static void showRectangles(Rectangle *arr, int n);

    static int calculateAreas(Rectangle *arr, int n);
};

#endif //DECOMPOSITION_RECTANGLE_H
