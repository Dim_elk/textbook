#include "rectangle.h"
#include <iostream>

Rectangle::Rectangle() {}

Rectangle::Rectangle(int a, int b) {
    this->a_ = a;
    this->b_ = b;
}

int Rectangle::getA() {
    return a_;
}

int Rectangle::getB() {
    return b_;
}

void Rectangle::setA(int a) {
    this->a_ = a;
}

void Rectangle::setB(int b) {
    this->b_ = b;
}

void Rectangle::set(int a, int b) {
    setA(a);
    setB(b);
}

void Rectangle::showRectangles(Rectangle *arr, int n) {
    for (int i = 0; i < n; i++) {
        std::cout << "I know about the rectangle # " << i
                  << ". It has: " << std::endl;
        std::cout << " a=" << arr[i].getA() << std::endl;
        std::cout << " b=" << arr[i].getB() << std::endl;
    }
}

int Rectangle::calculateAreas(Rectangle *arr, int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += arr[i].getA() * arr[i].getB();
    }
    return sum;
}