#ifndef INHERITANCE_MYVECTOR_H
#define INHERITANCE_MYVECTOR_H

#include <iostream>

const int MAX_SIZE = 5;

class MyVector {
public:
    MyVector(char *el = NULL, int maxsz = MAX_SIZE);

    MyVector(MyVector &v);

    ~MyVector();

    void add_element(char *el);

    bool delete_element(int i);

    char *operator[](int i);

    void sort();

    int Size() { return size; }

    int Maxsize() { return maxsize; }

    int find(char *el);

    MyVector &operator=(MyVector &v);

    friend std::ostream &operator<<(std::ostream &out, MyVector &v);

protected:
    int maxsize;
    int size;
    char **pdata;
private:
    void resize();
};

#endif //INHERITANCE_MYVECTOR_H
