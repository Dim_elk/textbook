---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
marp: true
math: mathjax
---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
code {
    background: #000;
}
pre {
    background: #000;
}
</style>

# Программирование на основе классов и шаблонов

<style scoped>
h1 {
    font-size: 1.5rem;
}
h2 {
    font-size: 1rem;
}
</style>

![bg left:40% 80%](./../../images/iu5edu_logo.jpg)

## Наследование

**Аладин** Дмитрий Владимирович

[cpp2.docs.iu5edu.ru](https://cpp2.docs.iu5edu.ru)

---

# План

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.9rem;
}
</style>

1. Ассоциация, композиция и агрегация
2. Понятие наследования. Виды и дерево наследования
3. Ключи наследования и правило доступа к членам производного класса
4. Правила наследования и примеры реализации методов классов при наследовании
5. Иерархия классов
6. Виртуальные функции. Ключевые слова override и final
7. Абстрактные классы и интерфейсы
8. Виртуальные конструкторы и деструкторы
9. Стратегии наследования

---

# Очередная [классификация](https://tproger.ru/experts/oop-in-simple-words/) - три кита ООП

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 1rem;
}
</style>

1. **Инкапсуляция** — способ спрятать сложную логику внутри класса, предоставив программисту лаконичный и понятный интерфейс для взаимодействия с сущностью.
```<--Вы находитесь здесь-->```
2. **Наследование** — способ легко и просто расширить существующий класс, дополнив его функциональностью.
3. **Полиморфизм** — принцип "один интерфейс — множество реализаций". Например, метод `print` может вывести текст на экран, распечатать его на бумаге или вовсе записать в файл.

---

# Эпичная цитата для придания важности лекции

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.9rem;
}
</style>

В ответ от 2007 года на то, почему C++ не использовался в Git вместо C:

> C++ - ужасный язык. Это становится еще более ужасным из-за того, что многие некачественные программисты используют его до такой степени, что с его помощью гораздо проще генерировать полное [продукт жизнедеятельности, и это не код]. Откровенно говоря, даже если бы выбор C ничего не делал, кроме как не подпускал программистов на C++, это само по себе было бы огромной причиной использовать C.
>
> [Линус Торвальдс, создатель Linux](https://lwn.net/Articles/249460/)

---

# [Аргумент](https://lwn.net/Articles/249460/) Линуса Торвальдса о том, почему C++ не так хорош

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

> C++ приводит к действительно плохому выбору дизайна. Вы неизменно начинаете использовать "приятные" библиотечные функции языка, такие как STL, Boost и прочую полную чушь, которая может "помочь" вам программировать, но вызывает:
>
> ...
>
> - **неэффективные абстрактные** модели программирования, когда через два года вы замечаете, что какая-то абстракция была не очень эффективной, но теперь весь ваш код зависит от всех окружающих его хороших объектных моделей, и вы не можете исправить это, не переписав свое приложение.

---

# Еще одна цитата по поводу того, что "киты" ООП не так хороши, как кажутся на первый взгляд

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
</style>

> Я думаю, что невозможность повторного использования затрагивает объектно-ориентированные, а не функциональные языки. Поскольку проблема с ООП-языками заключается в том, что у них есть вся эта неявная среда, которую они носят с собой. Вы хотели банан, а получили гориллу с бананом и целые джунгли в придачу.
>
> [Джо Армстронг, создатель Erlang](https://www.johndcook.com/blog/2011/07/19/you-wanted-banana/)

---

# Если речь пошла про языки...

<style scoped>
h1 {
    font-size: 0.8rem;
}
p {
    font-size: 0.8rem;
}
</style>

![bg left:70% 90%](images/programming-languages-mem.jpg)

Изображение создано и опубликовано, при поддержке [сообщества C#](https://vk.com/tnull?w=wall-72495085_1393574).

---

# Конкретная критика наследования ООП

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 1rem;
}
</style>

> **ООП-наследование не отражает наследование реального мира.**
>
> Родительский объект не может изменить поведение дочерних объектов во время выполнения. Даже если вы наследуете свою ДНК от родителей, они не могут вносить изменения в вашу ДНК по своему усмотрению. Вы не наследуете поведение от своих родителей. Вы развиваете своё поведение. И вы не можете переопределить поведение своих родителей.
>
> [Илья Суздальницкий, senior full-stack-разработчик](https://tproger.ru/translations/oop-the-trillion-dollar-disaster/)

---

# Рекомендация ["банды четырех"](https://ru.wikipedia.org/wiki/Design_Patterns)

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
</style>

![bg left:40% 95%](./images/gang-of-four.jpg)

> [Второе правило] объектно-ориентированного проектирования:
>
> предпочитайте **композицию** наследованию класса.
>
> В идеале для достижения повторного использования вообще не следовало бы создавать новые компоненты.
> ...  проектировщики злоупотребляют наследованием.

*P.S.* Некоторые современные языки программирования вообще его избегают [например, Go].

---

# Композиция??? Мы только про агрегацию и наследование говорили!

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 1rem;
}
</style>

![bg right:60% 100%](images/hold-up-mem.png)

Сейчас проясним!

---

# Что насчет [ассоциации](https://habr.com/ru/post/354046/)?

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
</style>

**Ассоциация** – это когда один класс включает в себя другой класс в качестве одного из полей. Ассоциация описывается словом "имеет". Автомобиль имеет двигатель. Вполне естественно, что он не будет являться наследником двигателя (хотя такая архитектура тоже возможна в некоторых ситуациях).

![height:380px center](images/car-mem.jpg)

---

# Два частных случая ассоциации: композиция и агрегация

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
</style>

Что это за Покемон?

```cpp
class Engine {
  int _power;

 public:
  explicit Engine(int p) { _power = p; }
};

class Car {
  string _model = "Porshe";
  Engine* _engine;

 public:
  Car() : _engine{new Engine(360)} {}
};
```

---

# Композиция

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
</style>

![bg right:50% 90%](images/composition-mem.jpg)

**Композиция** – это когда двигатель не существует отдельно от автомобиля. Он создается при создании автомобиля и полностью управляется автомобилем.

В типичном примере, экземпляр двигателя будет создаваться в **конструкторе** автомобиля.

---

# Агрегация

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
pre {
    font-size: 0.75rem;
}
</style>

**Агрегация** – это когда экземпляр двигателя создается где-то в другом месте кода, и передается в конструктор автомобиля в качестве параметра.

```cpp
class Engine {
  int _power;
 public:
  explicit Engine(int p) { _power = p; }
};

class Car {
  string _model = "Porshe";
  Engine* _engine;
 public:
  explicit Car(Engine* engine) { _engine = engine; }
};

int main() {
  auto engine = new Engine(360);
  Car car(engine);
  return 0;
}
```

---

# На этом закончим лекцию про наследование?

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8rem;
}
</style>

![height:520px center](images/catch-mem.jpg)

---

# В любой непонятной ситуации ссылайся на Роберта Мартина

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
</style>

![bg right:40%](images/robert-martin.jpg)

> **Когда наследование предпочтительнее композиции?**
>
> Сначала мы должны признать, что **наследование** - это всего лишь **особая форма композиции**. Производные классы состоят из своих базовых классов.
>
> Однако связь производного с базовым гораздо более тесная, чем связь композита с компонентом.
>
> [Роберт Мартин в голубой птичке](https://twitter.com/unclebobmartin/status/1308029538463625219)

---

# Open Source - это сила*

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
</style>

![bg left:50% 80%](./images/opensource-mem.jpeg)

При подготовке данной лекции использовался Open Source материал 😎

Основа материала от [@ashtanyuk](https://github.com/ashtanyuk/CPP-2019/blob/master/texts/07-Inheritance.md). Адаптированный материал публикуется с сохранением условий распространения.

*Акция действует только при наличии паспорта "хорошего программиста". Представителям "неправильных" компаний [просьба не беспокоить](https://habr.com/ru/news/t/722752/).

---

# Понятие наследования

<style scoped>
h1 {
    font-size: 1.1rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

**Наследование** - механизм передачи свойств одних классов другим классам в процессе проектирования иерархии классов. Исходные классы называют:

- **базовыми** (БК) (родителями);
- **производными** (ПК) (потомками).

![bg right:50% 90%](images/what-hell-mem.png)

---

# Виды наследования

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8rem;
}
</style>

Наследование может быть **одиночным** и **множественным**, в зависимости от числа непосредственных родителей у конкретного класса.

![height:380px center](images/types-of-inheritance.png)

Источник картинки [habr](https://habr.com/ru/post/463125/).

---

# Дерево наследования

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Для удобного изображения отношений классов при наследовании строят специальный граф: **дерево наследования**:

![height:290px center](images/inheritance-tree.svg)

Базовые классы изображают над производными.

---

# Пример с наследованием

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class Box  // тип ``коробка``
{
 protected:
  int _width, _height;

 public:
  void SetWidth(int w) { _width = w; }
  void SetHeight(int h) { _height = h; }
};
class ColoredBox : public Box  // ``цветная коробка``
{
  int _color;

 public:
  void SetColor(int c) { _color = c; }
};

```

---

# Ключи наследования

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

![bg left:40% 110%](images/keys-inheritance.jpg)

При описании производного класса в его заголовке перечисляются все базовые для него классы. Доступ к элементам этих классов регулируется с помощью ключей доступа:

- `private`;
- `protected`;
- `public`.
  
Если базовых классов несколько, они перечисляются через запятую.

---

# Правило доступа к членам производного класса

<style scoped>
h1 {
    font-size: 1.2rem;
}
tr {
    font-size: 0.8rem;
}
</style>

![bg right:40% 70%](images/keys-rule.jpg)

|  Ключ доступа     | Раздел БК | Раздел ПК |
|:-----------------:|:---------:|:---------:|
| private           | private   | нет       |
| private           | protected | private   |
| private           | public    | private   |
| protected         | private   | нет       |
| protected         | protected | protected |
| protected         | public    | protected |
| public            | private   | нет       |
| public            | protected | protected |
| public            | public    | public    |

---

# Читаем таблицу правила доступа к членам ПК

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 1rem;
}
</style>

- Если в БК некоторая переменная располагалась в разделе `public`, а ПК был объявлен с ключом `private`, то в ПК к данной переменной можно будет обращаться только членам ПК или его друзьям (эта переменная перейдет в раздел `private` ПК).
- Если наследование без явного указания спецификатора, все имена базового класса в производном классе автоматически становятся приватными (или можно указать `private`).
- Если наследовать с ключевым словом `public` - все общедоступные имена базового класса будут общедоступными в производном классе и все защищенные имена будут защищенными в производном классе.

---

# Правила наследования

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

![bg right:40% 90%](images/jason-statham-mem.jpg)

~~Я ВАМ ЗАПРЕЩАЮ~~
Нельзя наследовать:

- конструкторы;
- деструкторы;
- перегруженные `new`;
- перегруженные `=`;
- отношения дружественности.

---

# Правила для специальных методов

<style scoped>
h1 {
    font-size: 1rem;
}
li {
    font-size: 0.8rem;
}
</style>

- ПК должен иметь **собственные конструкторы**.
- Если в конструкторе производного класса явный вызов конструктора базового класса отсутствует, автоматически **вызывается конструктор БК**.
- Для иерархии, состоящей из нескольких классов, конструкторы БК вызываются начиная с **самого верхнего уровня**. После этого выполняются конструкторы элементов класса, являющихся объектами, а затем - конструктор класса.
- В случае нескольких БК их конструкторы вызываются **в порядке объявления**.
- **Для деструкторов** эти правила справедливы, но порядок вызова обратный - **сначала ПК, затем БК**. Не требуется явно вызывать деструкторы, поскольку они будут вызваны автоматически.

---

# Передача параметра в конструктор базового класса

<style scoped>
h1 {
    font-size: 1.1rem;
}
</style>

```cpp
class X {
  int _a, _b, _c;

 public:
  X(int x, int y, int z) : _a{x}, _b{y}, _c{z} {}
};
class Y : public X {
  int _val;

 public:
  Y(int d) : X(d, d + 1, d + 5) { _val = d; }
  Y(int d, int e);
};

Y::Y(int d, int e) : X(d, e, 12) { _val = d + e; }
```

---

# Пример работы с производными классами (`Employee`)

<style scoped>
h1 {
    font-size: 1.1rem;
}
</style>

```cpp
struct Date {
  int dd, mm, yy;
};

class Employee {
  string _name, _surname;
  Date _hire_date, _fire_date;

 public:
  Employee(string name, string surname);
  ~Employee();
  void hire(Date d);
  void fire(Date d);
  string name() const;
  string surname() const;
  void print() const;
};
```

---

# Пример работы с производными классами (`Programmer`)

<style scoped>
h1 {
    font-size: 1.1rem;
}
</style>

```cpp
class Programmer : public Employee {
  string _team;

 public:
  Programmer(string name, string surname, string team);
  ~Programmer();
  void set_team(string team);
  string team() const;
  void print() const;
};
```

---

# Пример доступных методов экземпляров классов `Employee` и `Programmer`

<style scoped>
h1 {
    font-size: 0.8rem;
}
</style>

```cpp
Employee::Employee()
Employee::~Employee()
Employee::hire()
Employee::fire()
Employee::name()
Employee::surname()
Employee::print()

Programmer::Programmer()
Programmer::~Programmer()
Programmer::set_team()
Programmer::team()
Programmer::print()

Programmer::Employee::hire()
Programmer::Employee::fire()
Programmer::Employee::name()
Programmer::Employee::surname()
Programmer::Employee::print()
```

---

# Пример использования методов экземпляров классов `Employee` и `Programmer`

<style scoped>
h1 {
    font-size: 0.8rem;
}
</style>

```cpp
Date start_date(1, 1, 2004), end_date(31, 12, 2007);

Employee emp("Vasya", "Pupkin");
emp.hire(start_date);
cout << emp.name() << " " << cout << emp.surname() << endl;
emp.print();
emp.fire(end_date);

Programmer prog("Petr", "Petrov", "GM00");
prog.hire(start_date);
prog.set_team("GM12");
cout << prog.name()  << " " << prog.surname() << " " <<  prog.team() << endl;
prog.print();
prog.Employee::print();
prog.fire(end_date);
```

---

# Производные классы и указатели

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.9rem;
}
pre {
    font-size: 0.9rem;
}
</style>

С объектом производного класса можно обращаться как с объектом базового класса при обращении к нему при помощи указателей и ссылок.

```cpp
Programmer *prog1 = new Programmer("Petr", "Petrov", "GM12");
Employee *emp1 = prog1;  // хорошо
Employee *emp2 = new Employee("vasya", "Pupkin");
Programmer *prog2 = emp2;  // ошибка. Значение типа "Employee *" не
// может быть использовано для инициализации объекта типа "Programmer *"
prog2->set_team("GM00");   // с чего бы это заработало, если ошибка выше?)

void test_function(Employee & emp);

Programmer prog3("Ivan", "Ivanov", "GM00");
test_function(prog3);  // хорошо
```

---

# Пример функций-членов класса (объявление классов)

<style scoped>
h1 {
    font-size: 1.1rem;
}
</style>

```cpp
class Employee {
  string _name, _surname;
  //...
 public:
  void print() const;
  string surname() const;
  //...
};

class Programmer : public Employee {
  string _team;
  //...
 public:
  void print_surname() const;
  void print_with_team() const;
  //...
};
```

---

# Пример функций-членов класса (реализация классов)

<style scoped>
h1 {
    font-size: 1.1rem;
}
</style>

```cpp
void Employee::print() const { cout << _surname << endl; }
void Programmer::print_surname() const { cout << surname() << endl; }
void Programmer::print_with_team() const {
  Employee::print();
  cout << _team << endl;
}

int main() {
  Employee emp("Vasya", "Ivanov");
  Programmer prog("Petr", "Petrov", "GM12");

  emp.print();  // Ivanov

  prog.print_surname();  // Petrov
  prog.print_with_team();  // "Petrov\nGM12"
}
```

---

# Пример реализации конструктора копирования

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class Employee {
 protected:
  string _name, _surname;
 public:
  Employee(const Employee&);
  Employee& operator=(const Employee&);
  //...
};
class Programmer : public Employee {
  string _team;
 public:
  Programmer(const Programmer&);
  Programmer& operator=(const Programmer&);
  //...
};
Programmer::Programmer(const Programmer& rp) : Employee{rp}, _team{rp._team} {}

Programmer& Programmer::operator=(const Programmer& rp) {
  Employee::operator=(rp);
  _team = rp._team;
  return *this;
}
```

---

# Пример иерархии классов (в виде дерева)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Рассмотрим небольшую иерархию должностей в софтверной компании:

![height:350px center](images/class-hierarchy.svg)

---

# Пример иерархии классов (объявление)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class Employee { /* (•‿•) */ };
class Programmer : public Employee { /* (シ_ _)シ */ };
class Team_leader : public Programmer { /* (－_－) zzZ */ };
class Proj_manager : public Employee { /* …ᘛ⁐̤ᕐᐷ */ };
class Senior_Manager : public Proj_manager { /* (,,◕　⋏　◕,,) */ };
class HR_manager : public Employee { /* ( ˘▽˘)っ♨ */ };
```

---

# Пример иерархии классов (конкретная реализация)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class Team_leader : public Programmer {
  list<Programmer*> _team_list;
 public:
  Team_leader(string n, string fn, string t);
  bool add_designer(Programmer*);
  bool rm_designer(string fn, string n);
  Programmer* get_designer(string fn, string n) const;
};

Team_leader::Team_leader(string n, string fn, string t)
    : Programmer(n, fn, t), _team_list() {}
```

---

# Пример иерархии классов (использование)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
Team_leader tm("Igor", "Kotov", "GM12");

tm.hire(Date(20, 3, 2008));
cout << tm.name();
tm.set_team("GM18");
tm.add_designer(p);
tm.fire(Date());
```

---

# Пример виртуальных функций (объявление)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class Employee {
 protected:
  string _name, _surname;
  Date _hire_date, _fire_date;

 public:
  Employee(string name, string surname);
  // ...
  virtual void print() const;
};

class Programmer : public Employee {
  string _team;

 public:
  Programmer(string name, string surname, string team);
  virtual void print() const override;
};
```

# Пример виртуальных функций (реализация)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
void Employee::print() const { cout << _name << " " << _surname << endl; }

void Programmer::print() const {
  Employee::print();
  cout << "team: " << _team << endl;
}
```

---

# Пример виртуальных функций (использование)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
void print_emp(const Employee* pEmp) {
  cout << "Employee info:" << endl;
  pEmp->print();
}

int main() {
  Employee emp("Vassya", "Pupkin");
  Programmer prog("Ivan", "Sidorov", "GM12");
  print_emp(&emp);
  print_emp(&prog);
  return 0;
}
```

---

# Пример таблицы виртуальных функций

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

![height:550px center](images/virtual-function-table.svg)

---

# Объяснение таблицы виртуальных функций

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Любой класс, который использует виртуальные функции (или дочерний класс, родительский класс которого использует виртуальные функции), имеет свою собственную **виртуальную таблицу**. Это обычный **статический массив**, который создается компилятором во время компиляции.

Виртуальная таблица содержит **по одной записи на каждую виртуальную функцию**, которая может быть вызвана объектами класса. Каждая запись в этой таблице — это **указатель на функцию, указывающий на метод, доступный объекту этого класса**.

---

# Объяснение таблицы виртуальных функций (продолжение)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Обычно компилятор создает отдельную `vtable` для каждого класса. После создания объекта указатель на эту `vtable`, называемый **виртуальный табличный указатель**, добавляется как скрытый член данного объекта (а зачастую как первый член).

Компилятор также генерирует "скрытый" код в конструкторе каждого класса для **инициализации указателей** его объектов адресами соответствующей `vtable`.

---

# Объяснение таблицы виртуальных функций (продолжение)

<style scoped>
h1 {
    font-size: 1.1rem;
}
p {
    font-size: 0.9rem;
}
</style>

Если виртуальная функция не переопределена в производном классе, `vtable` производного класса хранит адрес функции в родительском классе. Таблица `vtable` используется для получения доступа к адресу при вызове виртуальной функции.

Механизм `vtable` позволяет реализовать **динамическое связывание** в C++. [*Оп-оп, это мы к полиморфизму подобрались?*]

Когда мы связываем объект производного класса с указателем базового класса, переменная `vtbl` указывает на `vtable` производного класса. Это **присвоение гарантирует, что будет вызвана нужная виртуальная функция**.

---

# Как использовать виртуальные функции?

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
</style>

Механизм виртуальности используется, только когда виртуальная функция вызывается через указатель или ссылку на базовый класс:

```cpp
Employee emp("Vasya", "Pupkin");
Programmer prog("Ivan", "Sidorov", "GM12");
emp.print();   // нет, Employee::print()
prog.print();  // нет, Programmer::print()

void fn1(Employee *p) {
  p->print();  // да
}

void fn2(Employee &r) {
  r.print();  // да
}
```

---

# Пример работы вызова виртуальной функции

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class Employee {
  // ...
  virtual void bonus() const;
};

// массив указателей на Employee, размер
void give_a_bonus(Employee *list[], int size) {
  for (int i = 0; (i < size && list[i]); ++i) list[i]->bonus();
}
void create_lucky_list_and_give_bonus() {
  Employee **list = new (Employee *)[10];
  for (int i = 0; i < 10; ++i) list[i] = next_lucky_man();
  give_a_bonus(list);
}
```

---

# Другой пример использования виртуальных функций

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

```cpp
class Unit {
 public:
  virtual bool action() { return false; };
};
class Soldier : public Unit { /*...*/ };
class Tank : public Unit { /*...*/ };
class Mine : public Unit { /*...*/ };

class Field {
  int _unit_number;
  Unit **_units;
 public:
  Field();
  ~Field();
  void refresh_field();
  void turn();
  void move_to_end(Unit *);
  //...
};

void Field::turn() {
  for (int i = 0; i < _unit_number; ++i)
    if (_units[i]->action() != true) move_to_end(_units[i]);
}
```

---

# Еще один [пример](https://radioprog.ru/post/1276) виртуальных функций с подвохом

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

```cpp
class A {
 public:
  virtual std::string_view getName1(int x) { return "A"; }
  virtual std::string_view getName2(int x) { return "A"; }
};

class B : public A {
 public:
  // обратите внимание: параметр имеет тип short int
  virtual std::string_view getName1(short int x) { return "B"; }
  // обратите внимание: функция - константная
  virtual std::string_view getName2(int x) const { return "B"; }
};

int main() {
  B b{};
  A& rBase{b};
  std::cout << rBase.getName1(1) << std::endl;                          // A
  std::cout << rBase.getName2(2) << std::endl;                          // A
  std::cout << rBase.getName1(static_cast<short int>(1)) << std::endl;  // A
  return 0;
}
```

---

# Пытаемся починить пример

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class A {
 public:
  virtual std::string_view getName1(int x) { return "A"; }
  virtual std::string_view getName2(int x) { return "A"; }
};

class B : public A {
 public:
  // ошибка: член класса не может быть повторно объявлен 
  virtual std::string_view getName1(int x) { return "B"; }
  // ошибка: член класса не может быть повторно объявлен 
  virtual std::string_view getName1(int x) { return "B"; }
};
```

---

# Ключевое слово `override`

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Ключевое слово `override` [служит двум целям](https://stackoverflow.com/questions/18198314/what-is-the-override-keyword-in-c-used-for):

1. Это показывает начало кода, что "это виртуальный метод, который **переопределяет** виртуальный метод базового класса".
2. Компилятор также знает, что это переопределение, поэтому он может "**проверить**", что вы не изменяете/не добавляете новые методы, которые, по вашему мнению, являются переопределениями.

---

# Пример использования `override`

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class A {
 public:
  virtual std::string_view getName1(int x) { return "A"; }
  virtual std::string_view getName2(int x) { return "A"; }
  virtual std::string_view getName3(int x) { return "A"; }
};

class B : public A {
 public:
  // ошибка компиляции, функция не является переопределением
  virtual std::string_view getName1(short int x) override { return "B"; }
  // ошибка компиляции, функция не является переопределением
  virtual std::string_view getName2(int x) const override { return "B"; }
  // ok, функция является переопределением A::getName3(int)
  virtual std::string_view getName3(int x) override { return "B"; }
};
```

---

# Ключевое слово `final`

<style scoped>
h1 {
    font-size: 1.3rem;
}
</style>

При использовании в объявлении или определении виртуальной функции `final` спецификатор гарантирует, что функция является виртуальной, и указывает, что она не может быть переопределена производными классами. В противном случае программа [неправильно сформирована](https://en.cppreference.com/w/cpp/language/final) (генерируется ошибка времени компиляции).

*P.S.* Отмечайте виртуальные функции или как `virtual`, или как `override`, но не то и другое одновременно.

---

# Пример использования `final`

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
#include <string_view>

class A {
 public:
  virtual std::string_view getName() { return "A"; }
};

class B : public A {
 public:
  // обратите внимание на использование спецификатора final в следующей строке
  // - это делает эту функцию больше не переопределяемой
  // ok, переопределяет A::getName()
  std::string_view getName() override final { return "B"; }
};

class C : public B {
 public:
  // ошибка компиляции: переопределяет B::getName(),
  // которая объявлена конечной
  std::string_view getName() override { return "C"; }
};
```

---

# Абстрактные классы

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

![bg right:40% 90%](images/jason-statham-mem.jpg)

Существуют классы, в которых виртуальные функции не имеют реализации. Такие функции называются **чисто виртуальными**, а классы, в которых они находятся, - **абстрактными**.

~~Я ВАМ ЗАПРЕЩАЮ~~
Нельзя создать экземпляр абстрактного класса.

---

# А может все-таки можно создать экземпляр абстрактного класса?

<style scoped>
h1 {
    font-size: 1rem;
}
pre {
    font-size: 0.8rem;
}
</style>

```cpp
class Cosmetics {
 public:
  virtual void make_up() = 0; // =0 - спецификатор чисто виртуальной функции pure
  virtual void touch_up() = 0;
  virtual void remove() = 0;
};
class Lipstick : public Cosmetics {
 public:
  void make_up() override final{};
  virtual void touch_up() override final{};
  virtual void remove() override final{};
};

int main() {
  Lipstick lips;    // ок
  Cosmetics cosmo;  // ошибка
  return 0;
}
```

---

# Ограничения на использование абстрактных классов

<style scoped>
h1 {
    font-size: 1.1rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

Абстрактные классы [нельзя использовать для](https://learn.microsoft.com/ru-RU/cpp/cpp/abstract-classes-cpp?view=msvc-140):

- переменных и данных членов;
- типов аргументов;
- типов возвращаемых функциями значений;
- типов явных преобразований.

Если конструктор абстрактного класса вызывает чистую виртуальную функцию прямо или косвенно, результат не определен. Однако конструкторы и деструкторы абстрактных классов могут вызывать другие функции-члены.

---

# Интерфейсы

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Под **интерфейсом**, в узком смысле, понимается абстрактный класс, в котором:

- все методы чисто виртуальные;
- нет полей с данными;
- все методы открытые (`public`).

В случае наследования от интерфейса, класс **обязан** реализовать его методы, иначе он остается абстрактным.

---

# Виртуальные деструкторы

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

```cpp
class base {
 public:
  base() {}
  virtual ~base() = 0;
};

base::~base() { std::cout << "~base()" << std::endl; }

class derived : public base {
 public:
  derived() {}
  ~derived() { std::cout << "~derived()" << std::endl; }
};

int main() {
  derived aDerived;  // destructor called when it goes out of scope
}
// ~derived()
// ~base()
```

---

# Зачем нужны виртуальные деструкторы?

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Виртуальные деструкторы обеспечивают корректное освобождение ресурсов при применении `delete` к указателю на базовый класс.

Пустая реализация чистой виртуальной функции `~base` [гарантирует](https://learn.microsoft.com/ru-RU/cpp/cpp/abstract-classes-cpp?view=msvc-140), что для функции существует по крайней мере некоторая реализация. Без этого компоновщик создает неразрешенную ошибку внешнего символа для неявного вызова.

---

# Виртуальные конструкторы

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

На самом деле, таких не существует! Но если очень нужно:

```cpp
class Employee {
 public:
  virtual Employee* new_employee() { return new Employee(); }
  virtual Employee* clone() { return new Employee(*this); }
};
class Programmer {
 public:
  virtual Programmer* new_employee() { return new Programmer(); }
  virtual Programmer* clone() { return new Programmer(*this); }
};
```

---

# Стратегии наследования

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.8rem;
}
</style>

- Производный класс имеет **доступ к защищенным членам базового** (но только для объектов собственного типа);
- **Защищенные данные** приводят к проблемам сопровождения;
- **Защищенные функции** - хороший способ задания операций для использования в производных классах;
- **Открытое наследование** делает производный класс **подтипом базового**;
- **Защищенное** и **закрытое наследование** используются для выражения деталей реализации;
- **Защищенные базовые классы** полезны в иерархиях с дальнейшим наследованием;
- **Закрытые базовые классы** полезны для "ужесточения интерфейса".

---

# В следующих сериях

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Чуть позже обсудим множественное наследование!

![height:350px center](./images/multiple-inheritance.png)

*P.S.* Спойлеры [здесь](https://learn.microsoft.com/ru-ru/cpp/cpp/multiple-base-classes?view=msvc-160).

---

# Вопросы?

<style scoped>
p {
  text-align: center;
}
</style>

![height:450px center](../../images/questions4.jpeg)

**If not, just clap your hands!**
