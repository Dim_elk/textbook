---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
marp: true
math: mathjax
---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
code {
    background: #000;
}
pre {
    background: #000;
}
</style>

# Программирование на основе классов и шаблонов

<style scoped>
h1 {
    font-size: 1.5rem;
}
h2 {
    font-size: 1rem;
}
</style>

![bg left:40% 80%](./../../images/iu5edu_logo.jpg)

## Перегрузка операций и отношения между классами

**Аладин** Дмитрий Владимирович

[cpp2.docs.iu5edu.ru](https://cpp2.docs.iu5edu.ru)

---

# План

1. Перегрузка функций и способы её осуществления
2. Перегрузка методом класса унарных и бинарных операций
3. Перегрузка внешней функцией унарных и бинарных операций
4. Перегрузка операции преобразования типа
5. Перегрузка операторов new и delete
6. Отношения агрегация и наследование между классами

---

# Про накопление жизненного опыта и проект с практики

<style scoped>
h1 {
    font-size: 1.6rem;
}
p {
    font-size: 0.8rem;
}
</style>

![height:380px center](images/life-experience-mem.jpeg)

[Добро пожаловать в чудесный мир...](https://youtu.be/atIwagMBDi4)

---

# А как стать крутым?

![height:450px center](images/programmer-level.jpeg)

---

# Что вас ждёт в будущем?

<style scoped>
h1 {
    font-size: 1rem;
}
li {
    font-size: 0.8rem;
}
</style>

1. Архитектура автоматизированных систем обработки информации и управления
2. Программирование на основе классов и шаблонов
3. Электротехника
4. Модели данных
5. Базы данных
6. Системное программирование
7. Схемотехника дискретных устройств
8. Электроника
9. Вычислительные средства АСОИУ
10. Операционные системы

---

# Что вас ждёт в будущем? (продолжение)

<style scoped>
h1 {
    font-size: 1rem;
}
li {
    font-size: 0.8rem;
}
</style>

11. Сети и телекоммуникации
12. Описание процессов жизненного цикла АСОИУ
13. Сетевые технологии в АСОИУ
14. Технология мультимедиа
15. Технологии машинного обучения
16. Сетевое программное обеспечение
17. Методы поддержки принятия решений
18. Эксплуатация АСОИУ
19. Элементы управления в АСОИУ
20. Парадигмы и конструкции языков программирования
21. Автоматизация разработки и эксплуатации ПО

---

# Кто ты, воин?

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 1rem;
}
li {
    font-size: 0.8rem;
}
</style>

![bg left:60% 100%](./images/graduate.jpg)

Будущий:

- **Архитектор** автоматизированных систем управления и обработки данных
- **Инженер**, способный отвечать на современные технологические вызовы

---

# Напутствие 1

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.8rem;
}
p {
    font-size: 0.8rem;
}
</style>

![height:450px center](images/instruction-mem-1.jpg)

**Учите математику** и постарайтесь не забыть её к концу бакалавриата.
*P.S.* Понятно, что сложно, но все же =)

---

# Напутствие 2

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.8rem;
}
p {
    font-size: 0.8rem;
}
</style>

![bg right:60% 80%](./images/analysis-mem.jpg)

Используйте **анализ** происходящего вокруг (**рефлексию**) и прошлого опыта по отношению к текущим результатам (**ретроспективный анализ**).

---

# Напутствие 3

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.8rem;
}
p {
    font-size: 0.8rem;
}
</style>

Не забивайте на другие сферы!

![height:400px center](images/t-shape.png)

Подробнее про Т-образную концепцию в разработке [здесь](https://vc.ru/hr/176314-t-obraznye-navyki-v-razrabotke-programmnogo-produkta).

---

В идеале бы вообще стать "Ш-специалистом"...

![height:470px center](images/sh-mem.png)

![bg right:50% 90%](images/sh-specialist-mem.jpg)

---

# Вернёмся к теме

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
</style>

![bg left:60% 95%](./images/mem-bassed-on.jpeg)

Следующий материал не украл, а адаптировал 😎

Основа материала от [@ashtanyuk](https://github.com/ashtanyuk/CPP-2019/blob/master/texts/05-Overloading.md). Адаптированный материал публикуется с сохранением условий распространения.

---

# Да кто такой этот ваш ПЕРЕГРУЗКА?

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

![height:500px center](./images/overloading-mem.png)

---

# Перегрузка функций

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 1rem;
}
</style>

**Перегрузка** - это создание функции с тем же именем, но с отличающимся списком параметров. Иными словами, перегруженные функции *называются одинаково*, но отличаются *количеством или типами своих параметров*. Благодаря этому, компилятор их отличает друг от друга в точке вызова.

Перегрузка операций позволяет разработать **аналог операции для пользовательского типа** в виде функции с определенным набором параметров.

*P.S.* Про *пользовательские типы* начали говорить только в этой дисциплине, поэтому раньше перегрузка нас не интересовала.

---

# Способы осуществить перегрузку

Перегрузка может быть осуществлена

- в виде **функции-члена** класса (методом)
- в виде **глобальной функции** (обычной или дружественной классу)

Пример использования перегрузки при сложении матриц:

```cpp
Matrix a, b, c;
...
c = a + b;
```

---

# Правила перегрузки

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

Для успешного использования перегрузки, надо знать и соблюдать ряд правил:

- перегружать можно все имеющиеся операции, кроме
  - `?:` - тернарный оператор;
  - `::` - доступ к вложенным именам;
  - `.`  - доступ к полям;
  - `.*` - доступ к полям по указателю;
  - `sizeof`, `typeid` и операторы `cast`.

Подробнее [здесь](https://learn.microsoft.com/ru-ru/cpp/cpp/operator-overloading?view=msvc-160).

---

# Правила перегрузки (продолжение)

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

- **нельзя придумывать** свои операции, например `@`, `+++`, `)(`, `(_!_)` и др.
- при перегрузке **нельзя изменять** арность операции, ассоциативность, приоритет
- способы перегрузки унарных и бинарных операций **отличаются**
- хотя бы один из аргументов перегружаемых оператором должен **быть пользовательского типа**

---

# Щепотка определений

**Арность предиката**, операции или функции в математике — количество их аргументов или операндов в возможности осуществлять последовательное применение формулы

**Ассоциативность** (сочетательность) — свойство бинарной операции $\circ$, заключающееся в возможности осуществлять последовательное применение формулы $(x \circ y) \circ z = x \circ (y \circ z)$ в произвольном порядке к элементам $x$, $y$ и $z$.

---

# Правила перегрузки (продолжение)

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.8rem;
}
</style>

- следующие операторы можно перегрузить **только в качестве методов**:
  - `=` - присваивание;
  - `->` - доступ к полям по указателю;
  - `()` - вызов функции;
  - `[]` - доступ по индексу;
  - `->*` - доступ к указателю-на-поле по указателю;
  - операторы конверсии и управления памятью.

- следующие операторы можно перегрузить **только в виде внешних функций** (перегружаем методы класса `<iostream>`):
  - чтение из потока;
  - запись в поток.

---

# Правила перегрузки (продолжение)

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8rem;
}
</style>

![height:450px center](./images/overload-rules-mem.jpg)

Общие правила перегрузки операторов можно подсматривать на [learn.microsoft.com](https://learn.microsoft.com/ru-ru/cpp/cpp/general-rules-for-operator-overloading).

---
# Перегрузка методом класса: бинарные операции

<style scoped>
h1 {
    font-size: 1.2rem;
}
h2 {
    font-size: 1rem;
}
pre {
    font-size: 0.8rem;
}
</style>

Для перегрузки бинарных операций нужно создать метод, принимающий один аргумент - **ссылку на второй аргумент операции**.

```cpp
class Complex {
 private:
  double _re, _im;

 public:
  Complex(double re, double im) : _re(re), _im(im) {}
  Complex operator+(const Complex& c) { return Complex(_re + c._re, _im + c._im); }
};
```

В результате операции сложения возникает новый объект типа `Complex`.

---

# Перегрузка методом класса: бинарные операции (продолжение)

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
pre {
    font-size: 0.8rem;
}
</style>

Перегрузим присваивание:

```cpp
class Complex {
  ...
  Complex& operator=(const Complex& c) {
    _re = c._re;
    _im = c._im;
    return *this;
  }
  ...
};
```

Теперь можно воспользоваться двумя перегруженными операциями:

```cpp
Complex a(1.1, 2.2), b(3.3, 4.4), c(0.0, 0.0);
c = a + b;
```

---

# Перегрузка методом класса: бинарные операции (продолжение)

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.75rem;
}
pre {
    font-size: 0.75rem;
}
</style>

```cpp
...
Complex operator+(const Complex& c) { return Complex(_re + c._re, _im + c._im); }
...
```

Обратите внимание на то, как создается временный объект внутри метода `operator+`:

```cpp
return Complex(_re + c._re, _im + c._im);
```

Сравните с другим возможным примером:

```cpp
Complex temp(_re + c._re, Im + c._im);
return temp;
```

В результате выполнения обоих примеров, будет проведена оптимизация и создаваться лишний объект при копировании будет только один (с суммой).

---

# Пример программы с перегрузкой бинарных операций

<style scoped>
h1 {
    font-size: 1rem;
}
pre {
    font-size: 0.75rem;
}
</style>

```cpp
class Complex {
 private:
  int _re, _im;

 public:
  Complex(int re = 0, int im = 0) : _re(re), _im(im) { std::cout << "C(" << re << "," << im << ")" << std::endl; }
  Complex(const Complex& c) : _re(c._re), _im(c._im) { std::cout << "CC(" << _re << "," << _im << ")" << std::endl; }
  ~Complex() { std::cout << "D(" << _re << "," << _im << ")" << std::endl; }
  Complex& operator=(const Complex& c) {
    _re = c._re; _im = c._im;
    return *this;
  }
  Complex operator+(const Complex& c) {
    Complex temp(_re + c._re, _im + c._im);
    return temp;
  }
};

int main() {
  Complex a(1, 2), b;
  b = a + a;
}
```

---
# Результат выполнения программы

<style scoped>
h1 {
    font-size: 1.1rem;
}
</style>

![bg right:50% 100%](./images/hz-mem.jpg)

```bash
C(1,2)
C(0,0)
C(2,4)
D(2,4)
D(2,4)
D(1,2)
```

**А как это получилось???**

---

# Отладка программы из примера (шаг 1)

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

![height:550px center](./images/binary-example/step1.png)

---

# Отладка программы из примера (шаг 2)

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

![height:550px center](./images/binary-example/step2.png)

---

# Отладка программы из примера (шаг 3)

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

![height:550px center](./images/binary-example/step3.png)

---

# Отладка программы из примера (шаг 4)

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

![height:550px center](./images/binary-example/step4.png)

---

# Отладка программы из примера (шаг 5)

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

![height:550px center](./images/binary-example/step5.png)

---

# Отладка программы из примера (шаг 6)

<style scoped>
h1 {
    font-size: 1rem;
}
</style>

![height:550px center](./images/binary-example/step6.png)

---

# А компилятор-то умным оказался!

<style scoped>
h1 {
    font-size: 1.1rem;
}
p {
    font-size: 0.9rem;
}
li {
    font-size: 0.9rem;
}
</style>

![bg left:60% 100%](./images/garbage-mem.png)

Подробнее про управление памятью:

1. [Основы программирования: C++. Кувшинов Д.Р. Память](https://teccxx.neocities.org/mx1/memory)
2. [C++ Programming/Memory Management](https://en.wikibooks.org/wiki/C%2B%2B_Programming/Memory_Management)

---

# Перегрузка методом класса: унарные операции

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
pre {
    font-size: 0.9rem;
}
</style>

Поскольку в операции участвует только один операнд, то никаких внешних ссылок методу, реализующему операцию, передавать не нужно.

```cpp
Complex operator-() {
  Complex temp;
  temp.Re = -Re;
  temp.Im = -Im;
  return temp;
}
```

Для операций `++` и `--` также существуют две формы: **префиксная** и **постфиксная**, которые реализуются при перегрузке особым образом.

---

# Пример программы с перегрузкой унарных операций

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.9rem;
}
pre {
    font-size: 0.9rem;
}
</style>

```cpp
class Coord {
  int _x, _y, _z;

 public:
  Coord(int x, int y, int z) : _x(x), _y(y), _z(z){}
  ...
  // Перегрузка префиксной формы:
  Coord& operator++() {
    ++_x; ++_y; ++_z;
    return *this;
  }
  // Перегрузка постфиксной формы:
  Coord operator++(int) {
    Coord temp = *this;
    ++_x; ++_y; ++_z;
    return temp;
  }
  ...
};
```

---

# Перегрузка внешней функцией

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8rem;
}
</style>

В этом случае у нас есть также две возможности: создать обычную функцию, или функцию, **дружественную** классу. В случае **унарной** операции функция должна принимать ссылку на объект класса, к которому она применяется. Если операция **бинарная**, то таких ссылок должно быть две: на первый аргумент и на второй.

![height:350px center](./images/friend.jpg)

---

# Пример дружественной функции

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.9rem;
}
pre {
    font-size: 0.9rem;
}
</style>

```cpp
class Coord {
  int _x, _y, _z;

 public:
  Coord() {}
  Coord(int x, int y, int z) : _x(x), _y(y), _z(z) {}
  friend Coord operator+(const Coord&, const Coord&);
};

Coord operator+(const Coord& c1, const Coord& c2) {
  Coord temp;
  temp._x = c1._x + c2._x;
  temp._y = c1._y + c2._y;
  temp._z = c1._z + c2._z;
  return temp;
}

int main() {
  Coord a(1, 2, 3), b(4, 5, 6), c;
  c = a + b;
}
```

---

# Перегрузка присваивания

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.8rem;
}
li {
    font-size: 0.8rem;
}
</style>

Операция **присваивания** (aka **копирующая инициализация**) определена в любом классе по умолчанию как поэлементное копирование. Если класс содержит поля, память под которые выделяется **динамически**, необходимо определить собственную операцию присваивания. Чтобы сохранить семантику присваивания, функция должна **возвращать ссылку на объект**, для которого она вызвана и **принимать** в качестве параметра **ссылку на присваиваемый объект**.

- Оператор `=` можно перегружать **только методом класса**.
- Оператор `=` при наследовании **не наследуется**. Если подумать, то будет видно, что при наследовании могут быть добавлены новые поля или изменено их смысловое значение и следовательно оператор `=` может не работать корректно.

---

# Перегрузка потоков ввода/вывода

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.9rem;
}
</style>

Функции `operator<<` и `operator>>` должны быть **друзьями класса**, описывающего **пользовательский тип**.

Функции **принимают** в качестве аргументов **ссылку на входной (выходной) поток** и **ссылку на объект класса**, данные из которого необходимо обработать. В качестве **возвращаемых** значений необходимо также указывать **ссылки на поток** ввода или вывода (в этом случае можно конструировать сложные потоковые конструкции).

*P.S.* Будьте аккуратны при использовании `using namespace std`. Неосторожное использование может привести к веселому поиску ошибок компиляции при попытке реализовать перегрузку потоков ввода/вывода для вашего пользовательского типа.

---

# Пример перегрузки потоков ввода/вывода

<style scoped>
h1 {
    font-size: 1.2rem;
}
pre {
    font-size: 1rem;
}
</style>

```cpp
class Complex {
  ...
  friend ostream& operator<<(ostream& os, Complex& c);
  friend istream& operator>>(istream& is, Complex& c);
  ...
};
ostream& operator<<(ostream& os, Complex& c) {
  return os << '(' << c._re << ',' << c._im << ')';
}
istream& operator>>(istream& is, Complex& c) { return is >> c._re >> c._im; }
int main() {
  Complex c(0, 0);
  std::cin >> c;  // 23,45
  std::cout << c; // (23,45)
}
```

---

# Перегрузка операции преобразования типа

<style scoped>
h1 {
    font-size: 1.2rem;
}
pre {
    font-size: 1rem;
}
</style>

Преобразования типов позволяют задать правила преобразования нашего класса к другим типам и классам.

Также можно указать спецификатор `explicit`, который позволит преобразовывать типы только, если программист явно это указал (например `static_cast<Point3>(Point(2,3));)`.

Пример:

```cpp
Point::operator bool() const {
  return this->x != 0 || this->y != 0;
}
```

---

# Пример программы с перегрузкой операции преобразования типа

<style scoped>
h1 {
    font-size: 1rem;
}
p {
    font-size: 0.9rem;
}
pre {
    font-size: 0.9rem;
}
</style>

```cpp
class A {
  int x;

 public:
  A(int _x) : x(_x) {}

  operator int() const { return x; }
};

void foo() {
  A a;
  int b;
  b = a;
}
```

---

# Перегрузка `new` и `delete`

<style scoped>
h1 {
    font-size: 1.2rem;
}
</style>

Существует возможность перегрузить операторы `new`, `delete` для более эффективного распределения динамической памяти. Всего существует 4 формы операторов выделения и освобождения памяти

1. `new` - работа с одиночными объектами
2. `new[]` - работа с массивами объектов
3. `delete` - работа с одиночными объектами
4. `delete[]` работа с массивами объектов

---

# Перегрузка `new` и `delete` (продолжение)

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.9em;
}
p {
    font-size: 0.9em;
}
</style>

При перегрузке операторов для работы с динамической памятью нужно соблюдать следующие правила:

- операторам не требуется передавать параметр типа класс;
- первым параметром для `new` должен передаваться размер объекта `size_t`;
- возвращаемые значения `new` должны иметь тип `void*`;
- возвращаемые значения `delete`, `delete[]` должны иметь тип `void`; 
- первый аргумент `delete`, `delete[]` должен иметь тип `void*`;
- данные операции являются статическими членами класса.

---

# Работа перегрузки `new` и `delete`

<style scoped>
h1 {
    font-size: 1.2rem;
}
pre {
    font-size: 0.9em;
}
li {
    font-size: 0.9em;
}
p {
    font-size: 0.9em;
}
</style>

```cpp
class Foo {
  ... 
  void* operator new(size_t size);
  void operator delete(void* obj);
```

При вызове оператора `new` сначала выделяется память для объекта:

- Если выделение прошло **успешно**, то **вызывается конструктор**.
- Если конструктор выбрасывает **исключение**, то выделенная **память освобождается**.

При вызове оператора `delete` все происходит в обратном порядке: сначала вызывается деструктор, потом освобождается память. **Деструктор не должен выбрасывать исключений**.

---

# Работа перегрузки `new[]` и `delete[]`

<style scoped>
h1 {
    font-size: 1.2rem;
}
li {
    font-size: 0.8em;
}
p {
    font-size: 0.8em;
}
</style>

Когда оператор `new[]` используется для создания массива объектов, то сначала выделяется память для всего массива:

- Если выделение прошло **успешно**, то **вызывается конструктор** по умолчанию (или другой конструктор, если есть инициализатор) для каждого элемента массива начиная с нулевого.
- Если какой-нибудь конструктор выбрасывает **исключение**, то для всех созданных элементов массива **вызывается деструктор** в порядке, обратном вызову конструктора, затем выделенная память освобождается.

Для **удаления** массива надо вызвать оператор `delete[]`, при этом для всех элементов массива вызывается деструктор в порядке, обратном вызову конструктора, затем выделенная память освобождается.

---

# Особенности работы с `delete`

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8em;
}
</style>

Стандартные функции выделения памяти при невозможности удовлетворить запрос выбрасывают исключение типа `std::bad_alloc`

Любую форму оператора `delete` безопасно применять к нулевому указателю.

![height:400px center](./images/exception-mem.jpg)

---

# Пример программы с перегрузкой `new` и `delete`

<style scoped>
h1 {
    font-size: 1.2rem;
}
pre {
    font-size: 0.9em;
}
li {
    font-size: 0.9em;
}
p {
    font-size: 0.9em;
}
</style>

```cpp
class X {
  ...
 public:
  void* operator new(std::size_t size) {
    std::cout << "X new\n";
    return ::operator new(size);
  }

  void operator delete(void* ptr) {
    std::cout << "X delete\n";
    ::operator delete(ptr);
  }

  void* operator new[](std::size_t size) {
    std::cout << "X new[]\n";
    return ::operator new[](size);
  }

  void operator delete[](void* ptr) {
    std::cout << "X delete[]\n";
    ::operator delete[](ptr);
  }
};
```

---

# Перегрузка `new` и `delete` при наследовании

<style scoped>
h1 {
    font-size: 1.2rem;
}
pre {
    font-size: 1em;
}
p {
    font-size: 1em;
}
</style>

В классе (особенно, когда используется наследование) иногда удобно применить альтернативную форму функции освобождения памяти:

```cpp
void operator delete(void* p, std::size_t size);
void operator delete[](void* p, std::size_t size);
```

Параметр `size` задает размер элемента (даже в варианте для массива). Такая форма позволяет использовать разные функции для выделения и освобождения памяти в зависимости от конкретного производного класса.

---

# Ешё один пример программы с перегрузкой `new` и `delete`

<style scoped>
h1 {
    font-size: 1rem;
}
pre {
    font-size: 1em;
}
p {
    font-size: 1em;
}
</style>

```cpp
void *X::operator new(size_t size) {
  void *p;
  std::cout << "In overloaded new.";
  p = malloc(size);
  if (!p) {
    throw std::bad_alloc;  // Throw directly than with named temp variable
  }
  return p;
}

void X::operator delete(void *p) {
  std::cout << "In overloaded delete.\n";
  free(p);
}

void *X::operator new[](size_t size) {
  void *p;
  std::cout << "Using overload new[].\n";
  p = malloc(size);
  if (!p) {
    throw std::bad_alloc;
  }
  return p;
}

void X::operator delete[](void *p) {
  std::cout << "Free array using overloaded delete[]\n";
  free(p);
}
```

---

# Было сложно?

![height:500px center](./images/difficult-mem.png)

---

# Отношения между классами

В этом разделе мы обсудим два основных типа отношений между классами: **агрегацию** и **наследование**.

- **Агрегация** (*горизонтальное* отношение) предполагает, что один объект входит в состав другого.
- **Наследование** (*вертикальное* отношение) подразумевает, что один объект является разновидностью другого.

---

# Описание классов для примера

- В качестве первого класса рассмотрим `Engine` (двигатель). У двигателя есть определенная мощность, рабочее топливо и его расход (литры на 1 км).
- Вторым классом выступает `Tank` - топливный бак, который характеризуется емкостью. мы можем проверят, пуст ли бак, а также уменьшать количество топлива в нем на определенную величину.

---

# Класс для примера `Engine`

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 1em;
}
</style>

```cpp
#include <iostream>
using namespace std;

typedef unsigned short power_t;
enum Fuel { Petrol, Diesel };

class Engine {
 private:
  power_t _power;
  Fuel _fuel;
  double _consume;  // расход топлива
 public:
  Engine(power_t p, Fuel f, double c) {
    _power = p;
    _fuel = f;
    _consume = c;
  }
  double conFuel(size_t path) { return path * _consume; }
};
```

---

# Класс для примера `Tank`

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 1em;
}
</style>

```cpp
class Tank {
 private:
  double _capacity;

 public:
  Tank(double cap) : _capacity(cap) {}
  bool isEmpty() const { return _capacity <= 0.0; }
  void consume(double value) {
    if (!isEmpty()) _capacity -= value;
  }
};
```

---

# Пример агрегации

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8em;
}
pre {
    font-size: 0.8em;
}
</style>

При наличии двух узлов автомобиля попробуем создать класс `Auto`:

```cpp
class Auto {
 protected:
  Engine *_engine;
  Tank *_tank;

 public:
  Auto(Engine *e, Tank *t) : _engine(e), _tank(t) {}
  void move(size_t path) {
    size_t current = 0;
    while (current < path && _tank->isEmpty() == false) {
      _tank->consume(_engine->conFuel(1.0));
      cout << "Current dist: " << current << "km" << endl;
      current++;
    }
    cout << "Stop!" << endl;
  }
};
```

---

# Описание класса `Auto`

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8em;
}
pre {
    font-size: 0.8em;
}
</style>

Автомобиль содержит двигатель и бак и умеет передвигаться на некоторое расстояние, расходуя при этом горючее. При опустошении бака автомобиль останавливается.

Класс `Auto` выступает в качестве **базового класса** для нескольких типов автомобилей.

![height:300px center](./images/auto-mem.png)

---

# Пример наследования

Рассмотрим *легковой автомобиль* (`Car`):

```cpp
class Car : public Auto {
 protected:
  size_t _passengers;

 public:
  Car(Engine *e, Tank *t, size_t pass) : Auto(e, t) { _passengers = pass; }
};
```

Главная особенность нового класса: перевозка пассажиров.

---

# Пример наследования (продолжение)

<style scoped>
h1 {
    font-size: 1.2rem;
}
p {
    font-size: 0.8em;
}
pre {
    font-size: 0.8em;
}
</style>

Аналогично можно создать грузовик, перевозящий грузы:

```cpp
class Lorry : public Auto {
 protected:
  size_t _cargo;

 public:
  Lorry(Engine *e, Tank *t, size_t c) : Auto(e, t) { _cargo = c; }
};
```

В производных классах `Carr` и `Lorry` мы не определяем заново двигатель и бак - они переходят при наследовании от родительского класса `Auto`.

**Конструктор производного класса** должен вызвать **конструктор базового** и передать ему необходимые параметры.

---

# В следующей серии

Про наследование и не только!

![height:350px center](./images/inheritance-mem.jpeg)

---

# Вопросы?

<style scoped>
p {
  text-align: center;
}
</style>

![height:450px center](../../images/questions4.jpeg)

**If not, just clap your hands!**
