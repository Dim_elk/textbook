---
slug: 2023-02-16-workshop1
title: Основы Git (семинар)
authors: [aladin]
tags: [ИУ5-22Б, ИУ5-23Б, ИУ5-25Б]
---

Данный семинар проводился в формате мастер-класса по основам Git и работы с интерфейсом командной строки (CLI) git.

<!--truncate-->

Обсуждались следующие вопросы:

1. [Отличительные особенности Git](https://git.docs.iu5edu.ru/docs/lecture/git/distinctive-features)
2. [Базовые понятия](https://git.docs.iu5edu.ru/docs/lecture/git/basic-concepts)
3. [Основы работы с удаленным и локальным репозиторием](https://git.docs.iu5edu.ru/docs/lecture/git/basics/init)
