---
slug: 2023-03-18-vs-code
title: Дополнительная информация по работе с Visual Studio Code
authors: [aladin]
tags: [ИУ5-21Б, ИУ5-22Б, ИУ5-23Б, ИУ5-24Б, ИУ5-25Б, РТ5-21Б]
---

Здесь собрана информация, которая направлена на дополнение [информации](https://cpp1.docs.iu5edu.ru/docs/labs/lab1/Windows) об использовании Visual Studio Code в качестве IDE на разных платформах. Представленная информация не является рекомендацией к действию. Это закрепление промежуточного результата поиска решения для последующего дополнения основной статьи.

<!--truncate-->

## Полезные статьи

### Про отладку в Visual Studio Code

1. [Debug C++ in Visual Studio Code | code.visualstudio.com](https://code.visualstudio.com/docs/cpp/cpp-debug)
2. [Debugging | code.visualstudio.com](https://code.visualstudio.com/docs/editor/debugging)
3. [Основные сведения о кодировке файлов в VS Code и PowerShell | learn.microsoft.com](https://learn.microsoft.com/ru-ru/powershell/scripting/dev-cross-plat/vscode/understanding-file-encoding)
4. [CMake: Debug and launch | github.com/microsoft](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/debug-launch.md)

### Про UTF-8 и PowerShell

1. [UTF-8 Encoding (CHCP 65001) in PowerShell | delftstack.com](https://www.delftstack.com/howto/powershell/powershell-utf-8-encoding-chcp-65001/?utm_source=pocket_reader)
2. [Set Windows PowerShell to UTF-8 Encoding to Fix GBK Codec Can Not Encode Character Error – PowerShell Tutorial | tutorialexample.com](https://www.tutorialexample.com/set-windows-powershell-to-utf-8-encoding-to-fix-gbk-codec-can-not-encode-character-error-powershell-tutorial/?utm_source=pocket_saves)

## Дополнение к [Debug using a launch.json file](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/debug-launch.md#debug-using-a-launchjson-file)

`launch.json` для связки macOS + clang:

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "C/C++: clang++ build and debug active file",
            "type": "cppdbg",
            "request": "launch",
            // Resolved by CMake Tools:
            "program": "${command:cmake.launchTargetPath}",
            "args": [],
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}",
            "environment": [],
            "externalConsole": true,
            "MIMode": "lldb",
        }
    ]
}
```
