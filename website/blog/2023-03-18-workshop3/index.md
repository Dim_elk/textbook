---
slug: 2023-03-18-workshop3
title: О ключевых словах default, delete, const и рефакторинге (семинар)
authors: [aladin]
tags: [ИУ5-25Б]
---

На данном семинаре обсуждались возникшие вопросы о [лабораторной работе №3](/docs/labs/lab3) и [лабораторной работе №4](/docs/labs/lab4), использование ключевых слов `default`, `delete`, `const` и причины и последствия рефакторинга.

<!--truncate-->

## Способ повторного использования кода, использующегося в дружественных методах при наследованиях

Как говорилось в [лекции №5](/docs/lectures/classes) отношение дружественности не наследуется. Поэтому отношения дружественности в родительском и дочернем классе приходится указывать явно. Может возникнуть необходимость переиспользовать код, который используется в дружественных методах. Это можно сделать за счет выделения перенеиспользуемого кода в отдельный метод класса. Пример того, как это можно сделать, показан ниже. Обратите внимание, что для реализации этого метода использовалось ключевое слово `const`.

```cpp
#include <iostream>

class A {
 protected:
  double _val;
  std::ostream &print(std::ostream &out, const A &a) const;

 public:
  A() : _val{0} {}
  A(double val) : _val{val} {}

  friend std::ostream &operator<<(std::ostream &out, const A &a);
};

class B : public A {
  friend std::ostream &operator<<(std::ostream &out, const B &b);
};

std::ostream &A::print(std::ostream &out, const A &a) const {
  out << "Экземпляр класса имеет _val = " << a._val;
  return out;
}

std::ostream &operator<<(std::ostream &out, const A &a) {
  return a.print(out, a);
}

std::ostream &operator<<(std::ostream &out, const B &b) {
  return b.print(out, b);
}

int main() {
  setlocale(LC_ALL, "Russian");

  double val;
  std::cin >> val;

  const B b{val};

  std::cout << b << std::endl;

  return 0;
}
```

## Ключевые слова `default`, `delete`, `const`

### `const`

Предлагается к обсуждению код из [статьи Microsoft Learn](https://learn.microsoft.com/ru-ru/cpp/cpp/const-cpp), представленный ниже:

```cpp
#include <iostream>
// constant_member_function.cpp
class Date {
 public:
  Date(int mn, int dy, int yr);
  int getMonth() const;   // A read-only function
  void setMonth(int mn);  // A write function; can't be const
 private:
  int month;
};

int Date::getMonth() const {
  return month;  // Doesn't modify anything
}
void Date::setMonth(int mn) {
  month = mn;  // Modifies data member
}
int main() {
  Date MyDate(7, 4, 1998);
  const Date BirthDate(1, 18, 1953);
  MyDate.setMonth(4);     // Okay
  BirthDate.getMonth();   // Okay
  BirthDate.setMonth(4);  // C2662 (MSVC) or C/C++(1086) Error
}
```

### `delete`

Предлагается к обсуждению статьи:

1. [C++11 Language Extensions – Classes | isocpp.org](https://isocpp.org/wiki/faq/cpp11-language-classes)

### `default`

Предлагается к обсуждению статьи:

1. [Quick Q: How is “=default” different from “{}” for default constructor and destructor? | isocpp.org](https://isocpp.org/blog/2015/04/quick-q-how-is-default-different-from-for-default-constructor-and-destructo)

## О рефакторинге

Предлагается обсудить следующие статьи:

1. [Код как у сеньора: рефакторинг | tproger.ru](https://tproger.ru/articles/kod-kak-u-senora-refaktoring/)
2. [6 Efficient Things You Can Do to Refactor a C++ Project | cppstories.com](https://www.cppstories.com/2020/08/6things-refactor.html/)
3. [Что такое рефакторинг кода и зачем он нужен | skillbox.ru](https://skillbox.ru/media/code/chto_takoe_refaktoring_koda_i_zachem_on_nuzhen/)
4. [Рефакторинг — мощь сокрытая в качественном коде | habr.com](https://habr.com/ru/post/307762/)
