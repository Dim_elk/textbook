---
slug: 2023-04-04-qt6
title: О способе установить Qt6
authors: [aladin]
tags: [ИУ5-21Б, ИУ5-22Б, ИУ5-23Б, ИУ5-24Б, ИУ5-25Б, РТ5-21Б]
---

В данной статье представлен перечень полезных статей по установке Qt6.

<!--truncate-->

## Предварительные требования

- [Git](https://git-scm.com/) (>= 1.6.x)
- [CMake](https://cmake.org/) (>= 3.16, >= 3.18.4 for Ninja Multi-Config, >= 3.19 for WebEngine, >= 3.21.1 for static Qt builds in Qt 6.2+)
- [Ninja](https://ninja-build.org/)
- [Visual Studio 2022](https://visualstudio.microsoft.com/vs/).
- [Пакета Windows SDK | developer.microsoft.com](https://developer.microsoft.com/ru-ru/windows/downloads/windows-sdk/)

## Установка Qt6 и Qt Creator

Основная статья [Настраиваем окружение Qt6+QtC без VPN | habr](https://habr.com/ru/articles/709064/). Все перечисленные в статье действия необходимо выполнить.

## Установка отладчика

Далее переходим к настройке CDB отладчика. Получить CDB отладчик можно из [Пакета Windows SDK | developer.microsoft.com](https://developer.microsoft.com/ru-ru/windows/downloads/windows-sdk/). Скачайте установщик и выполните необходимые действия, которые предлагает установщик. Установщик нужно запускать от имени администратора.

## Настройка отладчика в Qt Creator

Используйте информацию из статьи [Setting Up Debugger | doc.qt.io](https://doc.qt.io/qtcreator/creator-debugger-engines.html).
