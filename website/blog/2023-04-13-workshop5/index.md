---
slug: 2023-04-13-workshop5
title: Шаблоны классов и функций (семинар)
authors: [aladin]
tags: [ИУ5-25Б]
---

На данном семинаре обсуждались вопросы, касающиеся описания шаблонов классов и функций и специализации шаблонов.

<!--truncate-->

На данном занятии рассматривались статьи:

1. [Просто о шаблонах C++ | habr.ru](https://habr.com/ru/articles/599801/)
2. [Шаблоны классов | radioprog.ru](https://radioprog.ru/post/1286)
3. [Шаблоны (C++) | learn.microsoft.com](https://learn.microsoft.com/ru-ru/cpp/cpp/templates-cpp?view=msvc-170)