---
slug: 2023-04-18-lubuntu-experiment
title: Эксперимент по разработке программ на C++ виртуальной машине с операционной системой Lubuntu   
authors: [aladin]
tags: [ИУ5-21Б, ИУ5-22Б, ИУ5-23Б, ИУ5-24Б, ИУ5-25Б, РТ5-21Б]
---

Данная статья содержит информацию об использовании виртуальной машины с операционной системой [Lubuntu](https://lubuntu.me/) для разработки программ на языке программирования С++.

<!--truncate-->

## Предварительные требования

### Технические требования

- **Минимум 15 Гб** свободного места на диске.
- **Минимум 4 Гб** оперативной памяти для запуска виртуальной машины без учета расходов на работу домашней операционной системы и ее программ. Вы можете попробовать самостоятельно уменьшить выделяемую оперативную память до 3 Гб, однако работоспособность при данной настройке не тестировалось.

### Программные требования

- Операционная система Windows 10 или Windows 11. Для иных операционных систем рекомендуется установить и использовать Visual Studio Code в домашней операционной системе.
- [Oracle VM VirtualBox](https://www.virtualbox.org/).
- Отключение виртуализации Hyper-V, если она включена. О том, как это сделано, смотрите в [статье](https://www.wintips.org/fix-virtualbox-running-very-slow-in-windows-10-11/). Если вы используете виртуализацию [Hyper-V](https://learn.microsoft.com/ru-ru/virtualization/hyper-v-on-windows/about/), то установку виртуальной машины с Lubuntu выполните самостоятельно.

## Актуальные образ образ OVA

По [ссылке](https://mega.nz/folder/kckHyR5R#fCqDszsG3drmDhG6B0e_eQ) доступен актуальный образ виртуальной машины в формате OVA. Он предназначен для импорта в [Oracle VM VirtualBox](https://www.virtualbox.org/).

## Как экспортировать OVA образ в Virtual Box

Смотрите в данной [статье](https://www.alphr.com/ova-virtualbox/).

## Установленные программы в виртуальной машине

- [Visual Studio Code](https://code.visualstudio.com/) со установленными рекомендованными [расширениями](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools-extension-pack).
- GCC из пакета [build-essential](https://packages.debian.org/sid/build-essential).
- [GDB](https://www.sourceware.org/gdb/).
- [SmartGit](https://www.syntevo.com/smartgit/).
