---
sidebar_position: 1
---

# Добавление текстового в файла в папку собранных исполняемых файлов при использовании CMake

Предположим, что вы в проекте лабораторной работы в директории `lab2` хотите расположить файл `source.txt`. Данный файл при сборке проекта вы хотите получить в той же папке, что и исполняемый файл в папке собранных исполняемых файлов. Иными словами, вы хотите данный файл увидеть вместе с исполняемым файлом. Чтобы CMake собирал проект необходимым нам образом, используйте команду [configure_file](https://cmake.org/cmake/help/latest/command/configure_file.html), как показано в примере ниже:

```cmake
cmake_minimum_required(VERSION 3.23)
project(lab2)

set(CMAKE_CXX_STANDARD 17)

// highlight-start
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/source.txt
        ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
// highlight-end

add_executable(lab2 main.cpp)
```

В отличие от [file(COPY ...)](https://cmake.org/cmake/help/latest/command/file.html), `configure_file` создает зависимость на уровне файла между вводом и выводом, то есть:

> Если входной файл изменен, система сборки повторно запустит CMake для перенастройки файла и повторной генерации системы сборки.

Эта информация взята из [ответа с stackoverflow.com](https://stackoverflow.com/questions/34799916/copy-file-from-source-directory-to-binary-directory-using-cmake).
