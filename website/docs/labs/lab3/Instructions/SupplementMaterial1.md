---
sidebar_position: 2
---


# Приложение 1

:::caution

1. Обратите внимание на то, что код ниже представляет собой интерактивный режим. Иными словами, данный пример можно использовать для реализации функции `interactive()`. На основе кода из функции `main()` данного примера вам необходимо разработать демонстрационный режим в виде функции `demo()`. Для этого вместо получения данных из `std::cin` задайте константной значение рассматриваемой дроби. В итоговой реализации функция `main()` должна выглядеть так, как это показано в разделе ["Режимы выполнения программы"](../../lab1/Modes/intro.md).
2. Обратите внимание на то, что при использовании кода из данного примера вы можете столкнуться с предупреждениями статического анализатора Cppcheck при тестировании вашего кода при запросе на слияние. В этом случае вам необходимо следовать указаниям Cppcheck и исправить код таким образом, чтобы пройти все проверки.

:::

```cpp
// Файл main.cpp
// Разработать класс обыкновенных дробей fraction
// (описание класса - файл fraction.h и реализацию методов класса - файл
// fraction.cpp) для выполнения приведенной ниже функции main(), в которой
// проверяются все предполагаемые варианты использования класса.
#include <iostream>
#include <iomanip>
#include "fraction.h"
#include "fraction.cpp"

int main() {
    setlocale(LC_ALL, "Russian");

    // ввод дроби с клавиатуры
    std::cout << "Введите дробь: \n";
    fraction z;
    std::cin >> z;
    std::cout << "z=" << z << std::endl;
    // проверка конструкторов
    fraction fr1(10, 14), fr2;
    std::cout << "fr2=" << fr2 << std::endl;
    std::cout << "fr1=" << fr1 << std::endl;
    fraction fr = "-1 4/8";
    std::cout << "fr=" << fr << std::endl;
    fraction x(z), y;
    std::cout << "x=" << x << std::endl;
    double dbl = -1.25;
    fraction f = dbl;
    std::cout << "f=" << f << std::endl;
    // проверка перегруженной операции "+"
    y = x + z;
    std::cout << "y=" << y << std::endl;
    y += x;
    f += dbl / 2;
    std::cout << "f=" << f << std::endl;
    y = x + dbl;
    std::cout << "y=" << y << std::endl;
    y = dbl + y;
    std::cout << "y=" << y << std::endl;
    y += dbl;
    std::cout << "y=" << y << std::endl;
    int i = 5;
    y += i;
    std::cout << "y=" << y << std::endl;
    y = i + x;
    std::cout << "y=" << y << std::endl;
    y = x + i;
    std::cout << "y=" << y << std::endl;
    y += dbl + i + x;
    std::cout << "y=" << y << std::endl;
    return 0;
}
```
