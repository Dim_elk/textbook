---
id: lab3-Instructions-intro
slug: /labs/lab3/Instructions/
sidebar_position: 1
---

# Введение

## Создание класса

В данной лабораторной работе необходимо создать класс для работы с обыкновенными дробями. Все операции, которые должны выполняться с дробями, включены в программу в [приложении 1](/docs/labs/lab3/Instructions/SupplementMaterial1).

Числитель и знаменатель дроби имеют тип `int`.

Дроби вводятся как строка, имеющая вид:

- для дробей с целой частью: знак, целая часть, пробел, числитель, слэш (`/`), знаменатель. Например: `-2 6/18`, `5 9/3`, `2 4/1`.
- для дробей без целой части: знак, числитель, слэш (`/`), знаменатель. Например: `3/4`, `-9/3`, `-8/6` (знаменатель всегда положительный).

Значения представленных выше дробей на экране при выводе должны иметь вид:

```text
-2 1/3, 8, 6.
3/4, -3, -1 1/3.
```

При выводе и после выполнения арифметических операций дроби сокращаются, то есть числитель и знаменатель не должны иметь общих множителей.

## Перегрузка операции

Для работы с дробями необходимо:

1. Перегрузить операции `+`, `+=` для сложения дробей и дроби и целого в любых сочетаниях (дробь+целое, целое+дробь, дробь+дробь).
2. Перегрузить операции `+`, `+=` для сложения дроби и `double` в любых сочетаниях (дробь+`double`, `double`+дробь). Преобразование `double`-дробь должно выполняться с точностью до `N_DEC` десятичных знаков после запятой, где `N_DEC` - целочисленная константа, задаваемая пользователем. Задайте значение по умолчанию `N_DEC=4`.

При перегрузке операций использовать *функции-члены класса*, а где это невозможно, то *функции-друзья класса*.

Для обеспечения более удобного контроля результатов выполнения программы вставьте в конструкторы и перегруженные операции операторы вывода, идентифицирующие выполняемую функцию.
Выполните следующий эксперимент: закомментируйте операции дроби с `int` и повторно выполните программу. Объясните результаты сложения дробей с целыми числами.

## Создание конструкторов

Для инициализации объектов разрабатываемого класса обыкновенных дробей предусмотреть соответствующие конструкторы (с одним аргументом типа `char*`, с одним аргументом типа `double` и с двумя аргументами типа `int`,
которые имеют значения по умолчанию).
