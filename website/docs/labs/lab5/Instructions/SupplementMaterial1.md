---
sidebar_position: 2
---

# Приложение 1. Описание программы

## Класс `MyVector`

Базовый класс `MyVector` является динамическим **массивом строк**. Размер вектора `maxsize` должен меняться в процессе выполнения программы следующим образом:

- если при добавлении элемента число элементов вектора `size` превысит размер вектора, `maxsize` увеличивается примерно в `1,5` раза (был `8`, станет `12`, если `size >= 8`)
- если при удалении элемента число элементов вектора `size` станет меньше `maxsize/2`, `maxsize` уменьшается примерно в `1,5` раза, но должен быть не меньше значения по умолчанию (был `12`, станет `8`, если `size < 6`). Новый элемент добавляется в конец вектора.

### Члены - данные (`protected`)

- `maxsize` - размер вектора;
- `size` - количество элементов в векторе;
- `pdata` - указатель, содержащий адрес динамического массива элементов (строк).

### Конструкторы и деструктор

- Конструктор с одним параметром (символьная строка) для создания множества размером `1`, который имеет значения по умолчанию и поэтому может использоваться для создания пустого множества;
- Конструктор копирования;
- Деструктор.

### Методы изменения

Класс `MyVector` должен реализовывать следующие функции:

- `add_element` – вставка элемента в конец вектора;
- `delete_element` – удаление элемента из произвольного места;
- `find(el)` – возвращает индекс элемента или `–1`, если элемент не найден;
- `resize` – изменение размера вектора `maxsize` при его переполнении или освобождении места (`private`);

### Операторы

- `[]` - для возврата элемента вектора (доступ по индексу);
- `=` - оператор присваивания.

## Класс `MySet`

Класс с именем `MySet` включает элементы, представленные ниже.

### Члены - данные

Все данные наследуются из класса `MyVector` (элементы множества хранятся в векторе).

### Конструкторы и деструкторы

Так как своих данных в `MySet` нет, то можно не перегружать имеющиеся (по умолчанию) конструктор копирования, оператор присваивания и деструктор.
При необходимости будут вызываться соответствующие элементы базового класса.

### Методы доступа

- `IsElement`, который даёт `true` , если строка-параметр есть в множестве, иначе даёт `false`. Для поиска элементов множества следует использовать метод половинного деления. Для его реализации разработать метод `q_find`, который имеет тип доступа `private`.

### Методы изменения

- `AddElement` - добавляет строку в множество, если её там ещё нет. Для ускорения поиска элементов создаваемое множество должно быть отсортировано по возрастанию значения ключа. Для сортировки при добавлении элементов использовать метод `sort()` базового класса.
- `DeleteElement` для удаления строки из множества, если она там есть

### Операторы

- операторы присваивания `-=`, `+=`, `*=`, где - означает разность, `+` объединение и `*` пересечение множеств.

### Функции – не члены класса (друзья класса)

- Перегруженная операция потокового вывода;
- Операторы `+` (объединение), `-` (разность), `\` (пересечение) и `==` (сравнение: истина, если элементы двух множеств совпадают).
  
  Примеры операций над множествами (приведены для целых чисел):

  ```text
  {1, 4, 5, 6} + {1, 2, 3, 4} => {1, 2, 3, 4, 5, 6}
  {1, 4, 5, 6} * {1, 2, 3, 4} => {1, 4}
  {1, 4, 5, 6} - {1, 2, 3, 4} => {5, 6}
  ```

## Заготовка программы

Заготовка программы с описанием классов `MyVector` и `MySet` содержится в репозитории по [ссылке](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/labs/lab5).

:::caution

Обратите внимание на то, что при использовании заготовки программы вы можете столкнуться с предупреждениями статического анализатора Cppcheck при тестировании вашего кода при запросе на слияние. В этом случае вам необходимо следовать указаниям Cppcheck и исправить код таким образом, чтобы пройти все проверки.

:::
