---
sidebar_position: 2
---

# Дополнительный материал

## Книги

1. Стивен Прата. Язык программирования С++. Лекции и упражнения.

## Документация

1. [Документация по языку C++ | learn.microsoft.com](https://learn.microsoft.com/ru-ru/cpp/cpp/?view=msvc-170)
2. [Курс С++ (2019-2020) | github.com/ashtanyuk](https://github.com/ashtanyuk/CPP-2019)

## Статьи

1. [Списки инициализаторов членов в конструкторах | radioprog.ru](https://radioprog.ru/post/1222)
2. [Списки инициализации в C++: хороший, плохой, злой | habr](https://habr.com/ru/post/330402/)
