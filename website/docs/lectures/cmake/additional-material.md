---
sidebar_position: 2
---

# Дополнительный материал

## Книги

1. [Документирование программ при структурной и объектной декомпозиции [Электронный ресурс] : электронное учебное издание : методические указания к выполнению практикума по программированию / Г. С. Иванова, Т. Н. Ничушкина ; МГТУ им. Н. Э. Баумана, Фак. "Информатика и системы управления", Каф. "Компьютерные системы и сети". - Москва : МГТУ им. Н. Э. Баумана, 2014. - 1 электрон. опт. диск (CD-ROM); 12 см.](https://e-learning.bmstu.ru/iu6/pluginfile.php/2978/mod_data/content/1647/Метод%20пособие%20Методы%20обработки%20данных%20и%20оценки%20программ.pdf)

## Видеозаписи

1. [002. Генерация правил сборки в CMake | Иван Сидоров](https://youtu.be/ckO98bRzL9Y)
2. [Сборка проектов на C++ с использованием CMake | OTUS](https://youtu.be/LZwEtbc9gEA)
3. [Статические и динамические библиотеки в С++ | TVDN](https://youtu.be/riXCPM6sFPs)
4. [CMake основы | SimpleCoding](https://youtu.be/SM3Klt2rY8g)
5. [CMake с нуля | SimpleCoding](https://youtube.com/playlist?list=PL6x9Hnsyqn2UwWjSvjCzAY6sEOBrHY7VH)

## Статьи

### Компиляция

1. [Introduction to C++ Compilation on the Command Line | unrealistic.dev](https://unrealistic.dev/posts/introduction-to-c-compilation-on-the-command-line)
2. [Complex C++ Compilation from the Command Line | unrealistic.dev](https://unrealistic.dev/posts/complex-c-compilation-from-the-command-line)

## Сборка проектов

1. [В чем набрать и чем собрать C++ проект | habr.com](https://habr.com/ru/post/442682/)
2. [Сборка проекта на С++ в GNU/Linux | habr.com](http://cs.mipt.ru/cpp/labs/lab1.html)

## Декомпозиция

1. [Структурирование и декомпозиция программы | uii.bitbucket.io](https://uii.bitbucket.io/study/courses/cs/lecture03_decomposition-2021.pdf)

### CMake

1. [Quick CMake tutorial | jetbrains.com](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html)
2. [Руководство по CMake для разработчиков C++ библиотек | Habr](https://habr.com/ru/post/683204/)
3. Полное руководство по CMake:
   1. [Часть первая: Синтаксис | itnan.ru](https://itnan.ru/post.php?c=1&p=431428)
   2. [Часть вторая: Система сборки | itnan.ru](https://itnan.ru/post.php?c=1&p=432096)
4. [Learn CMake's Scripting Language in 15 Minutes | preshing.com](https://preshing.com/20170522/learn-cmakes-scripting-language-in-15-minutes/)
5. [CMake Tutorial | neerc.ifmo.ru](https://neerc.ifmo.ru/wiki/index.php?title=CMake_Tutorial)
6. [Введение в CMake | habr.com](https://habr.com/ru/post/155467/)

### Анализ кода

1. [Тонкости анализа исходного кода C/C++ с помощью cppcheck | habr.com](https://habr.com/ru/post/210256/)
