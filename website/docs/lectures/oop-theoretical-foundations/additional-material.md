---
sidebar_position: 2
---

# Дополнительный материал

## Книги

1. [Мухортов В. В., Рылов В.Ю. Объектно-ориентированное программирование, аеализ и дизайн. Методическое пособие. Новосибирск, 2002.](http://ccfit.nsu.ru/~rylov/OOP&OOD.PDF)

## Статьи

1. [Основные понятия в объектно-ориентированном программировании ИЛИ моя шпаргалка по ООП | medium.com](https://medium.com/@annacherenshchykova/-98ffef738f1d)
2. [ООП в картинках | habr](https://habr.com/ru/post/463125/)
3. [Объектно-ориентированное программирование для самых маленьких | medium.com](https://medium.com/nuances-of-programming/объектно-ориентированное-программирование-для-самых-маленьких-b0e0578761f1)
4. [Cohesion и Coupling: отличия | habr](https://habr.com/ru/post/568216/)
5. [Low Coupling и High Cohesion | medium](https://medium.com/german-gorelkin/low-coupling-high-cohesion-d36369fb1be9)
6. [Изучаем C++. Часть 8. Библиотеки и пространства имён | skillbox.ru](https://skillbox.ru/media/code/izuchaem_c_chast_8_biblioteki_i_prostranstva_imyen/)
7. [Пространства имен (C++) | learn.microsoft.com](https://learn.microsoft.com/ru-ru/cpp/cpp/namespaces-cpp?view=msvc-160)
8. [Пространства имен. Ключевые слова namespace, using. Правила создания пространства имен. Глобальное пространство имен | bestprog.net](https://www.bestprog.net/ru/2022/04/17/c-namespaces-keywords-namespace-using-ru/)
9. [Зачем нужно понимать ООП | habr](https://habr.com/ru/post/479640/)
10. [OOAD – Объектно-ориентированные принципы | coderlessons.com](https://coderlessons.com/tutorials/akademicheskii/izuchite-obektno-orientirovannyi-analiz-i-dizain/ooad-obektno-orientirovannye-printsipy)
11. [Ликбез по типизации в языках программирования | habr.com](https://habr.com/ru/post/161205/)
