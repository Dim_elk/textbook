---
id: oop-theoretical-foundations-intro
slug: /lectures/oop-theoretical-foundations
sidebar_position: 1
--- 

# 4. Теоретические основы ООП

## План

- Сцепление и связанность проектов
- Пространство имен
- Объектно-ориентированное проектирование (OOD) и программирование (OOP)
- Главные и сопутствующие принципы объектно-ориентированного программирования (ООП)
- Объект, прототип и класс

## Материал

- [Презентация](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/lectures/oop-theoretical-foundations/presentation.pdf)
