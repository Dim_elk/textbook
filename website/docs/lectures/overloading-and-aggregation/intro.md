---
id: lecture-overloading-and-aggregation-intro
slug: /lectures/overloading-and-aggregation
sidebar_position: 1
--- 

# 6. Перегрузка операций и отношения между классами

## План

1. Перегрузка функций и способы её осуществления
2. Перегрузка методом класса унарных и бинарных операций
3. Перегрузка внешней функцией унарных и бинарных операций
4. Перегрузка операции преобразования типа
5. Перегрузка операторов new и delete
6. Отношения агрегация и наследование между классами

## Материал

- [Презентация](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/lectures/overloading-and-aggregation/presentation.pdf)
