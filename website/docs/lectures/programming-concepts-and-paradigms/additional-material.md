---
sidebar_position: 2
---

# Дополнительный материал

## Книги

1. [Парадигма программирования : курс лекций / Л. В. Городняя ; Новосиб. гос. ун-т. – Новосибирск : РИЦ НГУ, 2015. – 206 с.](https://www.iis.nsk.su/files/book/file/FIT-Gor-PP3.pdf)
2. [Рогов А.Ю. Основы процедурно-структурного программирования:
учебное пособие / А.Ю. Рогов, В.И. Халимон, О.В. Проститенко. – СПб.: СПбГТИ(ТУ), 2014. – 104 c](http://sa.technolog.edu.ru/repository/opsp_book.pdf)
3. [Г.И. Ревунков, Ю.Е. Гапанюк. ВВЕДЕНИЕ В ФУНКЦИОНАЛЬНОЕ И. ЛОГИЧЕСКОЕ ПРОГРАММИРОВАНИЕ. Электронное учебное издание – рабочая версия](https://ugapanyuk.github.io/data/fp.pdf)

## Методические пособия

1. [Понятие о парадигме программирования. Основные парадигмы программирования. Языки и парадигмы программирования](https://al.cs.msu.ru/system/files/20-Paradigms.pdf)

## Лекции

1. [Парадигмы программирования | Денис С. Мигинский](http://ccfit.nsu.ru/~shadow/DT6/pdf/lecture_2_1_paradigms.pdf)
2. [Парадигмы программирования (часть 1) | lectures.ostrov.ski](https://lectures.ostrov.ski/02-development/10-paradigms-1/)
3. [Парадигмы программирования (часть 2) | lectures.ostrov.ski](https://lectures.ostrov.ski/02-development/11-paradigms-2/)

## Видеозаписи

1. [Ликбез программиста #1: Парадигмы программирования - ООП, ФП... | EngineerSpock](https://youtu.be/4bs5vSg7EK8)
2. [История развития языков программирования | Timur Shemsedinov](https://youtu.be/qqz0VSaNxuw?list=PLHhi8ymDMrQZad6JDh6HRzY1Wz5WB34w0)
3. [Парадигмы программирования (обзор) | Timur Shemsedinov](https://youtu.be/Yk1sxLVHfjs?list=PLHhi8ymDMrQZad6JDh6HRzY1Wz5WB34w0)
4. [Поговорим про функциональное программирование | S0ER TALKS](https://youtu.be/rmqzxu1q-_Y)

## Статьи

1. [Парадигмы программирования | github.com](https://github.com/Bandydan/php/blob/master/23_Object-class_this-inheritanse_incapsulation.md)
2. [Что такое парадигмы программирования и зачем они нужны | skillbox.ru](https://skillbox.ru/media/code/chto_takoe_paradigmy_programmirovaniya/)
3. [Что такое парадигмы программирования и зачем они нужны | practicum.yandex.ru](https://practicum.yandex.ru/blog/paradigmy-programmirovaniya/)
4. [Как ООП помогает разработчикам писать код быстрее и проще | practicum.yandex.ru](https://practicum.yandex.ru/blog/obektno-orientirovannoe-programmirovanie/)
5. [Методы программирования | github.com](https://github.com/kolei/OAP/blob/master/articles/t2l2.md)
6. [Шесть парадигм программирования, которые изменят ваш взгляд на код | habr](https://habr.com/ru/company/productivity_inside/blog/327898/)
7. [Знакомство с функциональным программированием в Python, JavaScript и Java | medium.com](https://medium.com/nuances-of-programming/что-такое-функциональное-программирование-и-как-оно-реализовано-в-python-javascript-и-java-ed00ebc246df)
8. [Что такое логическое программирование и зачем оно нам нужно | habr.com](https://habr.com/ru/post/322900/)
9. [Что такое функциональное программирование | itstan.ru](https://itstan.ru/programmirovanie/chto-takoe-funktsionalnoe-programmirovanie.html)
10. [Появление ООП — реакция на кризис программного
обеспечения | artlib.osu.ru](http://artlib.osu.ru/Docs/piter/bookchap/978594723842.html)
