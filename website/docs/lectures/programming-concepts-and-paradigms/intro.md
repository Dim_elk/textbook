---
id: programming-concepts-and-paradigms-intro
slug: /lectures/programming-concepts-and-paradigms
sidebar_position: 1
--- 

# 3. Концепции и парадигмы программирования

## План

1. Понятия парадигма и концепция программирования
2. Классификации парадигм
3. Место парадигмы в разработке
4. Императивная парадигма и ее виды
5. Декларативная парадигма и ее виды
6. Выполнение императивных и декларативных программ
7. Объектно-ориентированная парадигма

## Материал

- [Презентация](https://gitlab.com/iu5edu/cpp-course-sem2/textbook/-/blob/main/src/lectures/programming-concepts-and-paradigms/presentation.pdf)
