---
sidebar_position: 2
---

# Дополнительный материал

## Статьи

1. [Просто о шаблонах C++ | habr.ru](https://habr.com/ru/articles/599801/)
2. [Принципы для разработки: KISS, DRY, YAGNI, BDUF, SOLID, APO и бритва Оккама | habr.ru](https://habr.com/ru/companies/itelma/articles/546372/)
3. [Специализация шаблона функции | radioprog.ru](https://radioprog.ru/post/1288)
4. [Специализация шаблона класса | radioprog.ru](https://radioprog.ru/post/1289)
5. [Наследование и шаблоны классов | metanit.com](https://metanit.com/cpp/tutorial/9.4.php)
