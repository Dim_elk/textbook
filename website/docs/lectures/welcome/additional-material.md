---
sidebar_position: 2
---

# Дополнительный материал

Электронные ресурсы:

- [Контроль версий. Git. CI и CD](https://git.docs.iu5edu.ru/)
- [Основы программирования](https://cpp1.docs.iu5edu.ru/)
