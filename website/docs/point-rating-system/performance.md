---
sidebar_position: 3
slug: /point-rating-system/performance
---

# Текущая успеваемость

Информацию по текущей успеваемости вы можете найти по [ссылке](https://e-learning.bmstu.ru/iu5/mod/url/view.php?id=1425).